﻿using System;
using System.IO;

namespace lab1algorytmjacksona
{
    public class Task
    {

        public Task(string filename)
        {
            string[] text = File.ReadAllLines(@"D:\REPO\SPD\lab1-algorytm-jacksona\data\" + filename); //cały text z pliku
            n = int.Parse(text[0]);

            r = new int[n + 1];
            p = new int[n + 1];
            c = new int[n + 1];

            r[0] = 0;
            p[0] = 0;
            c[0] = 0;

            for (int i = 1; i < n + 1; i++)
            {
                var entries = text[i].Split(' ');
                r[i] = int.Parse(entries[0]);
                p[i] = int.Parse(entries[1]);
            }
        }


        public int n { get; set; }
        public int[] r { get; set; }
        public int[] p { get; set; }
        public int[] c { get; set; }

        public int JacksonAlghoritm()
        {
            Array.Sort(r, p);

            for (int i = 1; i < n+1; i++)
            {
                var temp = Math.Max(c[i - 1], r[i]) + p[i];
                c[i] = temp;
            }

            return c[n];
        }

    }

    class Program
    {
        static void Main()
        {
            int number = 8;
            int correctAnswer = int.Parse(File.ReadAllText(@"D:\REPO\SPD\lab1-algorytm-jacksona\data\JACK" + number + @".OUT"));
            var task = new Task("JACK" + number + ".DAT");


            var solution = task.JacksonAlghoritm();

            Console.WriteLine(solution);
            Console.WriteLine(correctAnswer);

        }
    }
}
